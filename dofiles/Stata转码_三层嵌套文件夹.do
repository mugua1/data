/*
                         使用说明
在值标签不存在不可转换字符的前提下，将多个dta文件由gb18030转换至uft-8编码。
可能出现乱码的地方有：数据标签、变量名、变量内容、变量标签、值标签
其他的注意事项如下：
1. 在当前目录下生成了“backup files”子文件夹。
2. 将原dta数据保存至了“backup files”子文件夹中。
3. 转码后utf8格式的dta数据，覆盖了当前文件夹的原数据。
4. 此程序只能转一遍，只能转一遍，只能转一遍！否则原数据将丢失，且数据重新变回乱码。
5. 该程序会将当前文件夹、其子文件夹以及其子子文件夹下的所有dta文件进行上述操作（即三层嵌套）。
6. 如果文件夹内的dta文件存在不可转换的字符，则不可使用该代码进行转换，可跳至"转码_单个文件.do"或"转码_单个文件夹.do"寻找给出的解决方案。
7. 该do文件嵌套了"转码_单个文件夹.do"文件，依据下面命令，需将"转码_单个文件夹.do"放在D盘根目录下。
*/

*------------------------用户修改部分--------------------------------
cd "D:\stata15\ado\personal\Net_course"


*-------------------------无需变更部分--------------------------------
*将当前文件夹下的所有dta文件进行转码（第一层）
do "D:\转码_单个文件夹.do"

*将当前子文件夹下的所有dta文件进行转码（第二层）
local dir_list : dir . dirs "*", respectcase
foreach subdir of local dir_list {
	if "`subdir'" == "backup files" {
		continue //保留备份的原文件不动
	}
	cd "`subdir'"
	do "D:\转码_单个文件夹.do"
	
	*将当前子子文件夹下的所有dta文件进行转码（第三层）
	local dir_list2 : dir . dirs "*", respectcase
	foreach subdir2 of local dir_list2 {
		if "`subdir2'" == "backup files" {
			continue //保留备份的原文件不动
		}
		cd "`subdir2'"
		do "D:\转码_单个文件夹.do"
		qui cd ..
	}
	qui cd ..	
}
